package study.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import study.bean.DataAccessBean;
import study.bean.UserInfo;

@SuppressWarnings("serial")
@WebServlet(name = "FindAllServlet", urlPatterns = "/findall")
public class FindAllServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			// DataAccessBeanのインスタンスを作成
			DataAccessBean dab = new DataAccessBean();
			// DataAccessBeanのビジネスメソッド(findAllUserInfo()) を呼び出す
			Collection<UserInfo> userInfoList = dab.findAllUserInfo();
			// 取得したユーザ情報のコレクションをrequestスコープに格納する
			request.setAttribute("userInfoList", userInfoList);
			// /WEB-INF/list.jsp にForwardする
			RequestDispatcher rd = request
					.getRequestDispatcher("/WEB-INF/list.jsp");
			rd.forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
			// 例外が発生した場合はエラーページ（/WEB-INF/error.jsp）へフォワードする
			request.getRequestDispatcher("/WEB-INF/error.jsp").forward(request,
					response);
		}
	}

}
