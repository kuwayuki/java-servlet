package study.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
// TODO ssj_ex15 1_【全件表示一覧機能】手順3 @WebServletでurlにマッピングする
public class FindAllServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		// 以下のtryブロックのコメントを外し、tryブロックの処理を記述
		// try {
			// TODO ssj_ex15 1_【全件表示一覧機能】手順3 DataAccessBeanのインスタンスを作成

			// TODO ssj_ex15 1_【全件表示一覧機能】手順3 DataAccessBeanのビジネスメソッド(findAllUserInfo()) を呼び出す

			// TODO ssj_ex15 1_【全件表示一覧機能】手順3  取得したユーザ情報のコレクションをrequestスコープに格納する

			// TODO ssj_ex15 1_【全件表示一覧機能】手順3 /WEB-INF/list.jsp にForwardする


		// 以下のtryブロック エンドのコメントを外し、tryブロックを有効に
        // }

        // TODO ssj_ex15 1【全件表示一覧機能】以下の例外処理のコメントを外し、処理を有効に
        /*
		catch (SQLException e) {
			e.printStackTrace();
			// TODO ssj_ex15 1_【全件表示一覧機能】手順3  例外が発生した場合はエラーページ（/WEB-INF/error.jsp）へフォワードする

		}
		*/
	}

}
