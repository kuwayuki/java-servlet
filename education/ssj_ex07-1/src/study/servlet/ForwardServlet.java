package study.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
//TODO ssj_ex07-1 手順1 @WebServletアノテーションを設定
public class ForwardServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		// TODO ssj_ex07-1 手順2 /hello.jspを遷移先に指定してRequestDispatcherオブジェクトを取得

		// TODO ssj_ex07-1 手順2 Forward処理
	}
}
