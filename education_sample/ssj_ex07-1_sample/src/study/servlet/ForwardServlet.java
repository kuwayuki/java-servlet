package study.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
@WebServlet(name = "ForwardServlet", urlPatterns = "/hello")
public class ForwardServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		// /hello.jspを遷移先に指定してRequestDispatcherオブジェクトを取得
		RequestDispatcher rd = request.getRequestDispatcher("/hello.jsp");

		// Forward処理
		rd.forward(request, response);
	}
}
