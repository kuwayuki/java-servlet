package study.bean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

import javax.naming.NamingException;
import javax.sql.DataSource;


// TODO ssj_ex15 6_【二重登録チェック】手順1-a DuplicationEmailExceptionのimport

public class DataAccessBean {

	/** DataSource を保持する変数 */
	private static DataSource ds = null;

	/** lookup で使用するJNDI参照名 */
	private static final String JNDI_NAME = "java:comp/env/jdbc/ssjdb";

	/** DataSource 取得メソッド */
	private static DataSource getDataSource() throws NamingException {

		// TODO ssj_ex15 1_【全件表示一覧機能】手順2 getDataSourceメソッドの実装

		return ds;
	}

	/**
	 * 全件検索処理
	 *
	 * @return UserInfoオブジェクトのコレクション
	 * @throws SQLException
	 *             DB接続関連の障害が発生した場合
	 */
	public Collection<UserInfo> findAllUserInfo() throws SQLException {
        // TODO ssj_ex15 1_【全件表示一覧機能】手順2 Connection、PreparedStatement、ResultSet型変数の宣言




        try {
            // TODO ssj_ex15 1_【全件表示一覧機能】手順2 データ取得用SQL文

        	// TODO ssj_ex15 1_【全件表示一覧機能】手順2 戻り値用のCollectionオブジェクト
        	Collection<UserInfo> userList = null;

            // TODO ssj_ex15 1_【全件表示一覧機能】手順2 Connectionオブジェクトの取得

        	// TODO ssj_ex15 1_【全件表示一覧機能】手順2 PreparedStatementオブジェクトの生成

        	// TODO ssj_ex15 1_【全件表示一覧機能】手順2 SQL文の実行

        	// TODO ssj_ex15 1_【全件表示一覧機能】手順2 検索結果の取得
        	/* このコメントを外して処理を記述 ここから
            while (true) { // ← TODO ssj_ex15 1_【全件表示一覧機能】手順2 trueを削除し、繰返し条件を記入
                // TODO ssj_ex15 1_【全件表示一覧機能】手順2 UserInfoオブジェクトの生成

            	// TODO ssj_ex15 1_【全件表示一覧機能】手順2 各カラムの値を取得し、UserInfoオブジェクトにセットする

            	// TODO ssj_ex15 1_【全件表示一覧機能】手順2 テーブル一行分の結果を保持するUserInfoオブジェクトを
                // Collectionに追加する
            }
            このコメントを外して処理を記述 ここまで */

            // 戻り値の返却
            return userList;
        }
        // TODO ssj_ex15 1_【全件表示一覧機能】手順2 以下コメントを外し、例外処理を実装
        /*
        catch (NamingException e) {
            e.printStackTrace();
            throw new SQLException(e);
        }
        */
        finally {
            // TODO ssj_ex15 1_【全件表示一覧機能】手順2-b クローズ処理

        }
    }

	/**
	 * 新規登録処理
	 *
	 * @param userInfo
	 *            登録するユーザ情報
	 * @throws SQLException
	 *             DB接続関連の障害が発生した場合
	 * @throws DuplicateEmailException
	 *             既に存在するメールアドレスを挿入しようとした場合
	 */
	public void registUserInfo(UserInfo userInfo) throws SQLException {
		// TODO ssj_ex15 6_【二重登録チェック】手順1-a DuplicationEmailExceptionのthrowsをメソッド定義に追加
		// DuplicateEmailException {
		Connection conn = null;
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		ResultSet rs = null;
		try {
			// TODO ssj_ex15 2_【新規登録機能】手順1 データ登録用SQL文

			// TODO ssj_ex15 5_【二重登録チェック】手順1 二重登録チェック用SQL文

			// Connectionオブジェクトの取得
			conn = getDataSource().getConnection();
			// TODO ssj_ex15 5_【二重登録チェック】手順1 二重登録チェック用のPreparedStatementオブジェクトの生成

			// TODO ssj_ex15 5_【二重登録チェック】手順1 二重登録チェック用のパラメータ設定

			// TODO ssj_ex15 5_【二重登録チェック】手順1 二重登録チェック用の検索

			// TODO ssj_ex15 2_【新規登録機能】手順1 データ登録用のPreparedStatementオブジェクトの生成

			// TODO ssj_ex15 2_【新規登録機能】手順1 データ登録用のパラメータ設定

			// TODO ssj_ex15 2_【新規登録機能】手順1 データ登録用のSQL文の実行

		} catch (NamingException e) {
			e.printStackTrace();
			throw new SQLException(e);
		} finally {
			// TODO ssj_ex15 2_【新規登録機能】手順1 クローズ処理

			// TODO ssj_ex15 5_【二重登録チェック】手順1 ResultSetオブジェクトのクローズ
		}
	}

	/**
	 * 削除処理
	 *
	 * @param email
	 *            削除する項目のメールアドレス
	 * @throws SQLException
	 *             DB接続関連の障害が発生した場合
	 */
	public void deleteUserInfo(String email) throws SQLException {
		// Connection、PreparedStatement型変数の宣言
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			// TODO ssj_ex15 3_【削除機能】手順1 データ削除用SQL文

			// Connectionオブジェクトの取得
			conn = getDataSource().getConnection();
			// TODO ssj_ex15 3_【削除機能】手順1 PreparedStatementオブジェクトの生成

			// TODO ssj_ex15 3_【削除機能】手順1 パラメータの設定

			// TODO ssj_ex15 3_【削除機能】手順1 SQL文の実行

		} catch (NamingException e) {
			e.printStackTrace();
			throw new SQLException(e);
		} finally {
			// TODO ssj_ex15 3_【削除機能】手順1 クローズ処理

		}

	}
}
