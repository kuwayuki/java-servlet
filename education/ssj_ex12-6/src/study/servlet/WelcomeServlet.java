package study.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
@WebServlet(name = "WelcomeServlet", urlPatterns = "*.welcome", 
	initParams = { @WebInitParam(name = "encoding", value = "utf-8") })
public class WelcomeServlet extends HttpServlet {

    private String encoding;

    public void init() throws ServletException {
        // 初期化パラメータに設定された値を取得
        this.encoding = getInitParameter("encoding");
        System.out.println("init()が呼ばれた");
    }

    public void destroy() {
        System.out.println("destroy()が呼ばれた");
    }

    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        // リクエストパラメータの文字エンコーディングを指定（初期化パラメータに設定された値を利用）
        request.setCharacterEncoding(encoding);
        // リクエストパラメータ "userName" の値を取得
        String userName = request.getParameter("userName");

        // userName を"userName"という名前でrequestスコープに格納
        request.setAttribute("userName", userName);

        // /welcome.jsp へのforward処理
        RequestDispatcher rd = request.getRequestDispatcher("/welcome.jsp");
        rd.forward(request, response);
    }
}
