package study.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * クライアントから送信されるリクエストパラメーター"userName"を受信し、 レスポンスするServlet。
 */
@SuppressWarnings("serial")
// TODO ssj_ex05-2 @WebServletアノテーションを指定します
public class HelloServlet extends HttpServlet {

	@Override
	public void init() throws ServletException {
		 System.out.println("init()が呼ばれた");
	}
	
	@Override
	public void destroy() {
		 System.out.println("destroy()が呼ばれた");
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		// 出力オブジェクトを取得
		PrintWriter out = resp.getWriter();

		// HTMLを出力（レスポンス）
		out.println("<html>");
		out.println("<head>");
		out.println("<title>ssj_ex05-1</title>");
		out.println("</head>");
		out.println("<body>");
		out.println("Hello!");
		out.println("</body>");
		out.println("</html>");
	}
}
