package study.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;

@WebFilter(filterName = "CharacterEncodingFilter", urlPatterns = "/*", 
				initParams = @WebInitParam(name = "encoding", value = "utf-8"))
public class CharacterEncodingFilter implements Filter {

	private String encoding;

	@Override
	public void init(FilterConfig config) throws ServletException {
		// フィルタインスタンス生成直後に行いたい処理をここに記述
		this.encoding = config.getInitParameter("encoding");
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		// 毎回のリクエスト時に実行される処理をここに記述
		request.setCharacterEncoding(encoding);
		// ↓ 重要！！必ずdoFilterメソッドを呼び出すこと！！
		chain.doFilter(request, response);
		// 毎回のレスポンス時に実行される処理をここに記述
	}

	@Override
	public void destroy() {
		// フィルタインスタンス破棄直前に行いたい処理をここに記述
	}

}
