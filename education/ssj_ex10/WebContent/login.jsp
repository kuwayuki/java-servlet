<% // TODO ssj_ex10 手順3 pageディレクティブでMINEタイプと文字エンコーディングを指定 %>
<!DOCTYPE html>
<html>
<head>
<title>ログイン</title>
</head>
<body>
	<% // TODO ssj_ex10 手順3 EL式で『message』という名前のオブジェクトを表示 %>

	<% // TODO ssj_ex10 手順3 フォームの送信先とメソッドを設定 %>
	<form action="" method="">
		<table>
			<tr>
				<td>ユーザ名</td>
				<% // TODO ssj_ex10 手順3 ユーザ名入力欄の実装 %>
				<td><input type="" name="" /></td>
			</tr>
			<tr>
				<td>パスワード</td>
				<% // TODO ssj_ex10 手順3 パスワード入力欄の実装 %>
				<td><input type="" name="" /></td>
			</tr>
		</table>
		<input type="submit" value="ログイン" />
	</form>
</body>
</html>
