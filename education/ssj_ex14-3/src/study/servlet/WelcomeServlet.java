package study.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
@WebServlet(name = "WelcomeServlet", urlPatterns = "/welcome")
public class WelcomeServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html;charset=utf-8");

		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE html>");
		out.println("<html>");
		out.println("<head>");
		out.println("<title>Servletでのincludeの例</title>");
		out.println("<style type=\"text/css\"> .header_style {font-style: italic;font-weight:bolder;color: #FFFFFF;}</style>");
		out.println("</head>");
		out.println("<body>");

		// include処理
		RequestDispatcher rd = request.getRequestDispatcher("/header.jsp");
		rd.include(request, response);

		out.println("<br>");
		out.println("ここにコンテンツを記述");
		out.println("</body>");
		out.println("</html>");
	}
}
