<% // TODO ssj_ex15 2_【新規登録機能】手順3 pageディレクティブで、MIMEタイプと文字エンコーディングの指定 %>
<!DOCTYPE html>
<html>
<head>
<title>新規登録</title>
</head>
<body>
	新規登録
	<br>
	<br> ${ message }
	<br>
	<br>
	<% // TODO ssj_ex15 2_【新規登録機能】手順3 フォームの送信先は、コンテキストパスと"/regist"を連結したパス %>
	<form action="${ pageContext.request.contextPath }/regist"
		method="post">
		<% // TODO ssj_ex15 2_【新規登録機能】手順3 各入力項目をテキストボックスのname属性を指定 %>
		<table>
			<tr>
				<th>名前</th>
				<td><input type="text" name="" /></td>
			</tr>
			<tr>
				<th>読み</th>
				<td><input type="text" name="" /></td>
			</tr>
			<tr>
				<th>郵便番号</th>
				<td><input type="text" name="" /></td>
			</tr>
			<tr>
				<th>住所</th>
				<td><input type="text" name="" /></td>
			</tr>
			<tr>
				<th>電話番号</th>
				<td><input type="text" name="" /></td>
			</tr>
			<tr>
				<th>メールアドレス</th>
				<td><input type="text" name="" /></td>
			</tr>
		</table>
		<input type="submit" value="登録" />
	</form>
	<% // TODO ssj_ex15 2_【新規登録機能】手順3 一覧表示ページへのリンクを実装します。リンク先パスは、コンテキストパスと"/findall"を連結したパス %>
	<a href="${ pageContext.request.contextPath }/findall">一覧ページへ</a>
</body>
</html>
