package study.bean;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;

@SuppressWarnings("serial")
public class DataAccessBean implements Serializable {

    /**
     * 認証処理
     * @param userName ユーザ名
     * @param password パスワード
     * @return 認証成功の場合はtrue。認証失敗の場合はfalse
     * @throws SQLException DB接続関連の例外が発生した場合
     */
    public boolean authentication(String userName, String password)
            throws SQLException {
        // Connection、PreparedStatement、ResultSet 型のローカル変数の宣言
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            // 入力されたユーザ名をキーにしてデータを取得するSQL文
            String sql = "SELECT username, password FROM users WHERE username=?";

            // JDBCドライバのロード
            Class.forName("com.mysql.jdbc.Driver");
            // Connectionオブジェクトの取得
            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/ssjdb", "stu00", "stu00");
            // PreparedStatementオブジェクトの生成
            ps = conn.prepareStatement(sql);
            // パラメータの設定
            ps.setString(1, userName);
            // SQL文の実行
            rs = ps.executeQuery();
            // 検索結果の取得
            if (rs.next()) {
                // 検索結果から、パスワードを取得
                String selectedPassword = rs.getString("password");
                // 入力されたパスワードと、DBから取得したパスワードが一致しているかどうかを調べる
                if (!password.equals(selectedPassword)) {
                    // パスワードが一致しなかった場合は認証失敗。falseを返す。
                    return false;
                }
            } else {
                // 検索結果がなかった場合(ユーザ名が一致しない場合)は認証失敗。falseを返す。
                return false;
            }
            // 検索結果もあり、パスワードも一致した場合は認証成功。trueを返す。
            return true;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            throw new SQLException(e);
        } finally {
            // Connection、PreparedStatement、ResultSetオブジェクトのクローズ処理
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }
}
