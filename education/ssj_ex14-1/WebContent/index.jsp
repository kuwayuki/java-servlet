<%@ page contentType="text/html;charset=utf-8"%>
<!DOCTYPE html>
<html>
<head>
<title>includeディレクティブの例</title>
<style type="text/css">
.header_style {
	font-style: italic;
	font-weight:bolder;
	color: #FFFFFF;
}
</style>
</head>
<body>
	<%@ include file="header.jsp"%>
	<br> ここにコンテンツを記述
</body>
</html>
