package study.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

@WebFilter(filterName = "CharacterEncodingFilter", urlPatterns = "/*")
public class CharacterEncodingFilter implements Filter {

	@Override
	public void init(FilterConfig config) throws ServletException {
		// フィルタインスタンス生成直後に行いたい処理をここに記述
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		// 毎回のリクエスト時に実行される処理をここに記述
		request.setCharacterEncoding("utf-8");
		// ↓ 重要！！必ずdoFilterメソッドを呼び出すこと！！
		chain.doFilter(request, response);
		// 毎回のレスポンス時に実行される処理をここに記述
	}

	@Override
	public void destroy() {
		// フィルタインスタンス破棄直前に行いたい処理をここに記述
	}

}
