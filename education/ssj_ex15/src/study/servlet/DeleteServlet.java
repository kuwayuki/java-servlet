package study.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
//TODO ssj_ex15 3_【削除機能】手順2 @WebServletでurlにマッピングする
public class DeleteServlet extends HttpServlet {

	@Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

		// 以下のtryブロックのコメントを外し、tryブロックの処理を記述
		// try {
            // TODO ssj_ex15 3_【削除機能】手順2 ブラウザからのリクエストの文字エンコーディングを指定


        	// TODO ssj_ex15 3_【削除機能】手順2 リクエストパラメータ値を取得


            // TODO ssj_ex15 3_【削除機能】手順2 DataAccessBeanのインスタンスを作成


            // TODO ssj_ex15 3_【削除機能】手順2 DataAccessBeanのビジネスメソッド(deleteUserInfo()) にemailを渡して呼び出す


            // TODO ssj_ex15 3_【削除機能】手順2 /findall（一覧表示機能）にRedirectする



		// 以下のtryブロック エンドのコメントを外し、tryブロックを有効に
        // }

        // TODO ssj_ex15 3_【削除機能】手順2 以下の例外処理のコメントを外し、処理を有効に
        /*
        catch (SQLException e) {
            e.printStackTrace();
            // 例外が発生した場合はエラーページ（/WEB-INF/error.jsp）へフォワードする
            request.getRequestDispatcher("/WEB-INF/error.jsp").forward(request, response);
        }
        */
    }

}
