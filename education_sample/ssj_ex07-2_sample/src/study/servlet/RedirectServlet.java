package study.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
@WebServlet(name="RedirectServlet",urlPatterns="/hello")
public class RedirectServlet extends HttpServlet {

	protected void doGet(
			HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        // /ssj_ex07-2/hello.jspを遷移先に指定してRedirect処理
        response.sendRedirect("/ssj_ex07-2_sample/hello.jsp");

    }
}