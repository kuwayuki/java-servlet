package study.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import study.bean.DataAccessBean;

// TODO ssj_ex11 手順3 記述内容を確認してください
@SuppressWarnings("serial")
@WebServlet(name="LoginServlet", urlPatterns="/login")
public class LoginServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        try {
            // クライアントからの入力値を取得
            String userName = request.getParameter("userName");
            String password = request.getParameter("password");
            // DataAccessBeanのインスタンスを作成
            DataAccessBean dab = new DataAccessBean();
            // DataAccessBeanのビジネスメソッド(authentication()) に入力値を渡して呼び出す
            if (dab.authentication(userName, password)) {
                // 認証成功の場合

                // 次の画面で表示するために、入力されたユーザ名をrequestスコープに入れる
                request.setAttribute("userName", userName);

                // /WEB-INF/welcome.jsp にForwardする
                RequestDispatcher rd = request
                        .getRequestDispatcher("/WEB-INF/welcome.jsp");
                rd.forward(request, response);
            } else {
                // 認証失敗の場合
                // ログインエラーのメッセージ文字列をrequestスコープに入れる
                request.setAttribute("message", "ユーザ名またはパスワードが間違っています");
                // /login.jsp にForwardする
                RequestDispatcher rd = request
                        .getRequestDispatcher("/login.jsp");
                rd.forward(request, response);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new ServletException(e);
        }
    }
}