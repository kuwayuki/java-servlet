<%@ page contentType="text/html;charset=utf-8"%>
<!DOCTYPE html>
<html>
<head>
<title>includeアクションの例</title>
</head>
<style type="text/css">
.header_style {
	font-style: italic;
	font-weight:bolder;
	color: #FFFFFF;
}
</style>
<body>
	<jsp:include page="/header.jsp" flush="false" />
	<br> ここにコンテンツを記述
</body>
</html>
