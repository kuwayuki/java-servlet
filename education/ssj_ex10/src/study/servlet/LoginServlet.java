package study.servlet;

import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import study.bean.DataAccessBean;

@SuppressWarnings("serial")
// TODO ssj_ex10 手順1 @WebServletアノテーションを設定
public class LoginServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			// TODO ssj_ex10 手順1 クライアントからの入力値を取得

			
			// TODO ssj_ex10 手順1 DataAccessBeanのインスタンスを作成

			// TODO ssj_ex10 手順1 DataAccessBeanのビジネスメソッド(authentication()) に入力値を渡して呼び出す
			// 下記のDriverManager.getConnection("");は演習開始時に消してください。
			DriverManager.getConnection("");
			if (true) {
				// 認証成功の場合

				// TODO ssj_ex10 手順1 次の画面で表示するために、入力されたユーザ名をrequestスコープに入れる

				
				// TODO ssj_ex10 手順1 /WEB-INF/welcome.jsp にForwardする
				
			} else {
				// 認証失敗の場合
				// TODO ssj_ex10 手順1 ログインエラーのメッセージ文字列をrequestスコープに入れる

				// TODO ssj_ex10 手順1 /login.jsp にForwardする
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new ServletException(e);
		}
	}
}