package study.bean;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;

@SuppressWarnings("serial")
public class DataAccessBean implements Serializable {

    /**
     * 認証処理
     * @param userName ユーザ名
     * @param password パスワード
     * @return 認証成功の場合はtrue。認証失敗の場合はfalse
     * @throws SQLException DB接続関連の例外が発生した場合
     */
    public boolean authentication(String userName, String password)
            throws SQLException {
        // TODO ssj_ex10 手順2 Connection、PreparedStatement、ResultSet 型のローカル変数の宣言

    	
    	
    	try {
            // TODO ssj_ex10 手順2 入力されたユーザ名をキーにしてデータを取得するSQL文

    		
            // TODO ssj_ex10 手順2 JDBCドライバのロード
    		Class.forName("");

    		// TODO ssj_ex10 手順2 Connectionオブジェクトの取得
    		
            // TODO ssj_ex10 手順2 PreparedStatementオブジェクトの生成

    		// TODO ssj_ex10 手順2 パラメータの設定

    		// TODO ssj_ex10 手順2 SQL文の実行

    		// TODO ssj_ex10 手順2 検索結果の取得
            if (true) {
                // TODO ssj_ex10 手順2 検索結果から、パスワードを取得

            	// TODO ssj_ex10 手順2 入力されたパスワードと、DBから取得したパスワードが一致しているかどうかを調べる
                if (true) {
                    // パスワードが一致しなかった場合は認証失敗。falseを返す。
                    return false;
                }
            } else {
                // TODO ssj_ex10 手順2 検索結果がなかった場合(ユーザ名が一致しない場合)は認証失敗。falseを返す。
                return false;
            }
            // 検索結果もあり、パスワードも一致した場合は認証成功。trueを返す。
            return true;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            throw new SQLException(e);
        } finally {
            // TODO ssj_ex10 手順2 Connection、PreparedStatement、ResultSetオブジェクトのクローズ処理

        }
    }
}
