<%@ page contentType="text/html;charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<title>送信内容</title>
<style type="text/css">
table {
	border: 0px black solid;
}
.tr_top {
	border-width: 1px 0px;
	border-color: black;
	border-style: double;
	display: block;
}
tr{
display: table-row-group;
}

</style>
</head>
<body>
	<table>
		<c:forEach var="userInfo" items="${userInfoList}">
			<tr class="tr_top">
				<td>お名前</td>
				<td><c:out value="${ userInfo.name }" /><br></td>
			</tr>
			<tr>
				<td>読み</td>
				<td><c:out value="${ userInfo.yomi }" /><br></td>
			</tr>
			<tr>
				<td>郵便番号</td>
				<td><c:out value="${ userInfo.zip }" /><br></td>
			</tr>
			<tr>
				<td>住所</td>
				<td><c:out value="${ userInfo.address }" /><br></td>
			</tr>
			<tr>
				<td>電話番号</td>
				<td><c:out value="${ userInfo.tel }" /><br></td>
			</tr>
			<tr class="td_bottom">
				<td>メールアドレス</td>
				<td><c:out value="${ userInfo.email }" /><br></td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>
