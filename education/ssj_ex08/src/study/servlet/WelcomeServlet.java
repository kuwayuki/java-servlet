package study.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
//TODO ssj_ex08 手順2 @WebServletアノテーションを設定
public class WelcomeServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		// TODO ssj_ex08 手順2 リクエストパラメータの文字エンコーディングを指定
		
		// TODO ssj_ex08 手順2 リクエストパラメータ "userName" の値を取得
		

		// TODO ssj_ex08 手順2 userName を"userName"という名前でrequestスコープに格納
		

		// TODO ssj_ex08 手順2 /welcome.jsp へのforward処理

	}
}
