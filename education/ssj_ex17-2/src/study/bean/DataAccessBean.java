package study.bean;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

@SuppressWarnings("serial")
public class DataAccessBean implements Serializable {

	// DataSource を保持する変数
	private static DataSource ds = null;

	// lookup で使用するJNDI参照名
	private static final String JNDI_NAME = "java:comp/env/jdbc/ssjdb";

	/** DataSource 取得メソッド */
	private static DataSource getDataSource() throws NamingException {
		if (ds == null) {
			// InitialContext オブジェクトの生成
			InitialContext ic = new InitialContext();
			// lookup() メソッドにJNDI参照名を渡して、JNDIリソースを取得
			ds = (DataSource) ic.lookup(JNDI_NAME);
		}
		return ds;
	}

	/**
	 * 認証処理
	 * 
	 * @param userName
	 *            ユーザ名
	 * @param password
	 *            パスワード
	 * @return 認証成功の場合はtrue。認証失敗の場合はfalse
	 * @throws SQLException
	 *             DB接続関連の例外が発生した場合
	 */
	public boolean authentication(String userName, String password)
			throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			// 入力されたユーザ名をキーにしてデータを取得するSQL文
			String sql = "SELECT username, password FROM users WHERE username=?";

			// DataSourceの取得
			DataSource datasource = getDataSource();
			// Connectionオブジェクトの取得
			conn = datasource.getConnection();

			// PreparedStatementオブジェクトの生成
			ps = conn.prepareStatement(sql);
			// パラメータの設定
			ps.setString(1, userName);
			// SQL文の実行
			rs = ps.executeQuery();
			// 検索結果の取得
			if (rs.next()) {
				// 検索結果から、パスワードを取得
				String selectedPassword = rs.getString("password");
				// 入力されたパスワードと、DBから取得したパスワードが一致しているかどうかを調べる
				if (!password.equals(selectedPassword)) {
					// パスワードが一致しなかった場合は認証失敗。falseを返す。
					return false;
				}
			} else {
				// 検索結果がなかった場合(ユーザ名が一致しない場合)は認証失敗。falseを返す。
				return false;
			}
			// 検索結果もあり、パスワードも一致した場合は認証成功。trueを返す。
			return true;
		} catch (NamingException e) {
			e.printStackTrace();
			throw new SQLException();
		} finally {
			// クローズ処理
			if (rs != null) {
				rs.close();
			}
			if (ps != null) {
				ps.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
	}
}
