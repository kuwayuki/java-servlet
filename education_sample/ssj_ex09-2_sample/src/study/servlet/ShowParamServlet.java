package study.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import study.bean.UserInfo;

@SuppressWarnings("serial")
@WebServlet(name = "ShowParamServlet", urlPatterns = "/showparam")
public class ShowParamServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		// リクエストパラメータの文字エンコーディングを指定
		request.setCharacterEncoding("utf-8");
		// リクエストパラメータの値を取得
		String strNum = request.getParameter("selectNum");

		// Bean を"userInfoList"という名前でrequestスコープに格納
		request.setAttribute("userInfoList", this.getUserinfo(strNum));

		// /WEB-INF/showparam.jsp へのforward処理
		RequestDispatcher rd = request
				.getRequestDispatcher("/WEB-INF/showparam.jsp");
		rd.forward(request, response);
	}

	/**
	 * 表示するユーザ情報を生成
	 */
	private List<UserInfo> getUserinfo(String strNum) {

		List<UserInfo> userInfoList = new ArrayList<UserInfo>();

		Integer num = Integer.valueOf(strNum);

		for (int i = 0; i < num; i++) {

			UserInfo userInfo = new UserInfo();

			userInfo.setName(i + "番目の氏名");
			userInfo.setYomi(i + "番目のよみ");
			userInfo.setAddress("東京都品川区1-" + (i + 1));
			userInfo.setEmail("email" + i + "@mail.com");
			userInfo.setTel("(+0" + i + ")03-1234-5678");
			userInfo.setZip("00" + i + "-0000");
			userInfoList.add(userInfo);
		}

		return userInfoList;

	}
}
