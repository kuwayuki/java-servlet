<% // TODO ssj_ex15 1_【全件表示一覧機能】手順4 MINEタイプと文字エンコーディングの指定 %>
<% // TODO ssj_ex15 1_【全件表示一覧機能】手順4 taglibディレクティブでJSTLのコア機能の利用宣言 %>
<!DOCTYPE html>
<html>
<head>
<title>一覧</title>
</head>
<body>
	一覧
	<table border="1">
		<tr>
			<th>名前</th>
			<th>読み</th>
			<th>郵便番号</th>
			<th>住所</th>
			<th>電話番号</th>
			<th>メールアドレス</th>
			<th>削除</th>
		</tr>

		<% // TODO ssj_ex15 1_【全件表示一覧機能】手順4 "userInfoList"という名前でrequestスコープに格納したデータを、forEachタグで全要素繰り返し取り出す %>
			<tr>
			<% // TODO ssj_ex15 1_【全件表示一覧機能】手順4 EL式を使って、取り出したUserInfoオブジェクトのプロパティを出力 %>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td>
				<% // TODO ssj_ex15 3_【削除機能】手順3 削除ボタン %>
				</td>
			</tr>

	</table>
	<% // TODO ssj_ex15 1_【全件表示一覧機能】手順4 新規登録ページへのリンク %>
	<a href="">新規登録ページへ</a>
</body>
</html>
